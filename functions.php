<?php
/**
 * highscore functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package highscore
 */

/**
 * Define constants for paths, globals, etc. 
 */

define( 'THEMEROOT', get_template_directory_uri() );
define( 'THEME_DIST', THEMEROOT . '/dist' );
define( 'THEME_ASSETS', THEMEROOT . '/dist/assets' );

if ( ! function_exists( 'hs_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function hs_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on highscore, use a find and replace
		 * to change 'hs' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'hs', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'hs' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'hs_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( array(
				'title' => 'Общие поля',
				'slug' => 'common_fields',
			) );
		}

		/**
		 * Register custom image sizes
		 */
		add_image_size( 'features-laptop-content', 576, 362, true );
		add_image_size( 'teacher-photo', 295, 440, true );
		add_image_size( 'video-preview', 1030, 590, true );
	}
endif;
add_action( 'after_setup_theme', 'hs_setup' );

function hs_scripts() {
	wp_enqueue_style( 'hs-style', THEME_DIST . '/css/index.css', [], null );
	wp_register_script( 'hs-commons', THEME_DIST . '/js/commons.js', [], null, true );
	wp_register_script( 'hs-scripts', THEME_DIST . '/js/index.js', ['hs-commons'], null, true );
	wp_register_script( 'youtube-api', 'https://www.youtube.com/player_api', ['hs-scripts'], null, true );

	hs_localize_data();

	wp_enqueue_script( 'hs-commons' );
	wp_enqueue_script( 'hs-scripts' );
	wp_enqueue_script( 'youtube-api' );
}
add_action( 'wp_enqueue_scripts', 'hs_scripts' );

/**
 * Move jQuery to the footer. 
 */
function wpse_173601_enqueue_scripts() {
    wp_scripts()->add_data( 'jquery', 'group', 1 );
    wp_scripts()->add_data( 'jquery-core', 'group', 1 );
    wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );
}
add_action( 'wp_enqueue_scripts', 'wpse_173601_enqueue_scripts' );

if ( ! function_exists( 'hs_localize_data' ) ) :
function hs_localize_data() {
	/**
	 * Localize data for our bundles and scripts
	 */
	$path_array = [
		'themeAssets' => THEME_ASSETS,
		'ajax'  => admin_url( 'admin-ajax.php' ),
	];
	wp_localize_script( 'hs-commons', 'wpPaths', $path_array );
	
	$wc_products = wc_get_products( [
		'category' => [ 'courses' ],
	] );
	$products = [];
	foreach ( $wc_products as $product ) {
		$products[$product->get_id()] = [
			'price' => $product->get_price()
		];
	}
	
	$shared_data = [
		'products' => $products,
	];

	wp_localize_script( 'hs-commons', 'wpData', $shared_data );
}
endif; 

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Hooks that enhance the theme.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Custom theme shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Theme's util functions
 */
require get_template_directory() . '/inc/utils.php';

/**
 * Payment hook
 */
require get_template_directory() . '/inc/payment.php';