<?php
/**
 * Regsiter theme's handlers for various hooks
 *
 * @package highscore
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function hs_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'hs_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function hs_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'hs_pingback_header' );

/**
 * Remove auto wrapping in P tags from Contact Form 7 plugin if present 
 */
add_filter('wpcf7_autop_or_not', '__return_false');

/**
 * Fix svg in media library 
 */
function hs_check_filetype_and_ext( $data, $file, $filename, $mimes ) {
    
    global $wp_version;
    if ( $wp_version !== '4.7.1' ) {
        return $data;
    }

    $filetype = wp_check_filetype( $filename, $mimes );

    return [
        'ext'             => $filetype['ext'],
        'type'            => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];

}
add_filter( 'wp_check_filetype_and_ext', 'hs_check_filetype_and_ext', 10, 4 );

function hs_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'hs_mime_types' );

function hs_fix_svg() {
    echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
                width: 100% !important;
                height: auto !important;
        }
        </style>';
}
add_action( 'admin_head', 'hs_fix_svg' );

function hs_change_logo_class( $html ) {
    $html = str_replace( 'custom-logo', 'custom-logo__img responsive-img', $html );
    $html = str_replace( 'custom-logo-link', 'custom-logo', $html );

    return $html;
}
add_filter( 'get_custom_logo', 'hs_change_logo_class' );

/**
 * Add Custom Sizes to admin media library
 * 
 */

function hs_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, [
		'features-laptop-content' => __( 'Контент для слайдера с ноутбуком', 'hs' ),
		'teacher-photo'           => __( 'Фото преподавателя', 'hs' ),
		'video-preview'           => __( 'Видео первью', 'hs' ),
	 ] );
}
add_filter( 'image_size_names_choose', 'hs_custom_image_sizes' );
