<?php
/**
 * Payment hook
 *
 * @package highscore
 */


function getRobokassaForm($order_id, $label, $commission = 0) {

    $mrhLogin = get_option('MerchantLogin');

    if (get_option('test_onoff') == 'true') {
        $pass1 = get_option('testshoppass1');
        $pass2 = get_option('testshoppass2');
    } else {
        $pass1 = get_option('shoppass1');
        $pass2 = get_option('shoppass2');
    }

    $rb = new RobokassaPayAPI($mrhLogin, $pass1, $pass2);

    $order = wc_get_order($order_id);

    $sno = get_option('sno');
    $tax = get_option('tax');

    $receipt = array();

    if ($sno != 'fckoff') {
        $receipt['sno'] = $sno;
    }

    global $woocommerce;

    $cart = $woocommerce->cart->get_cart();

    foreach ($cart as $item) {
        $product = wc_get_product($item['product_id']);

        $current['name'] = $product->get_title();
        $current['quantity'] = (float) $item['quantity'];
        $current['sum'] = $item['line_total'];

        if (isset($receipt['sno']) && ($receipt['sno'] == 'osn')) {
            $current['tax'] = $tax;
        } else {
            $current['tax'] = 'none';
        }

        $receipt['items'][] = $current;
    }

    $order_total = floatval($order->get_total());

    if (get_option('paytype') == 'true') {
        if (get_option('who_commission') == 'shop') {
            DEBUG("who_commisson = shop");

            $commission = $commission / 100;
            DEBUG("commission = $commission");

            $incSum = number_format($order_total * (1 + (0 * $commission)), 2, '.', '');
            DEBUG("incSum = $incSum");

            $commission = $rb->getCommission($label, $incSum) / 100;
            DEBUG("commission = $commission");

            $sum = $rb->getCommissionSum($label, $incSum);
            DEBUG("sum = $sum");
        } elseif (get_option('who_commission') == 'both') {
            $aCommission = get_option('size_commission') / 100;
            DEBUG("who_commisson = both");
            DEBUG("aCommission = $aCommission");

            $commission = $commission / 100;
            DEBUG("commission = $commission");

            $incSum = number_format($order_total * (1 + ($aCommission * $commission)), 2, '.', '');
            DEBUG("incSum = $incSum");

            $commission = $rb->getCommission($label, $incSum) / 100;
            DEBUG("commission = $commission");

            $sum = $rb->getCommissionSum($label, $incSum);
            DEBUG("sum = $sum");
        } else {
            DEBUG("who_commission = client");
            $sum = number_format($order_total, 2, '.', '');
            DEBUG("sum = $sum");
        }
    } else {
        DEBUG("paytype = false");
        $sum = number_format($order_total, 2, '.', '');
        DEBUG("sum = $sum");
    }

    $invDesc = implode(', ', array_map(function(WC_Order_Item_Product $item) {
        return $item->get_name();
    }, $order->get_items()));

    if (strlen($invDesc) > 100) {
        $invDesc = "Заказ номер $order_id";
    }

    $receiptForForm = (get_option('type_commission') == 'false') ? $receipt : array();
    return $rb->createForm($sum, $order_id, $invDesc, get_option('test_onoff'), $label, $receiptForForm);
}

function hs_admin_course_payment() {
    global $woocommerce;

    ob_start();

    $first_name = isset( $_POST['first_name'] ) ? sanitize_text_field( $_POST['first_name'] ) : '';
    $last_name = isset( $_POST['last_name'] ) ? sanitize_text_field( $_POST['last_name'] ) : '';
    $email = isset( $_POST['user_email'] ) ? sanitize_email( $_POST['user_email'] ) : ''; 
    $product_id = isset( $_POST['product'] ) ? absint( $_POST['product'] ) : '';
    $quantity = isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1;

    $fields = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'email' => $email,
        'product_id' => $product_id,
        'quantity' => $quantity
    ];

    $invalid_fields = [];
    foreach ( $fields as $field_name => $field ) {
        if ( empty($field) ) {
            $invalid_fields[] = $field_name;
        }
    }

    if ( count($invalid_fields) > 0 ) {
        wc_add_notice('Ошибка в одном из полей. Пожалуйста, проверьте поля еще раз.', 'error');
        wp_send_json_error( [
            'fields' => $invalid_fields
        ] );
        wp_die();
    }

    $order = wc_create_order();
    $order->add_product( get_product( $product_id ), $quantity );
    $order->calculate_totals();
    $form = getRobokassaForm($order->id, 'all', 0);

    wp_send_json_success( [
        'product' => $product_id,
        'form' => $form
    ] );

    wp_die();
}
add_action( 'wp_ajax_course_payment', 'hs_admin_course_payment' );
add_action( 'wp_ajax_nopriv_course_payment', 'hs_admin_course_payment' );


