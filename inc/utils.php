<?php 
/**
 * Util functions used inside tempaltes
 * 
 * @package highscore
 */

 /**
  * Output a sprite icon with given icon_id and other attributes 
  * @param array options array
  *
  */
function hs_sprite_icon( $args ) {
	$attributes = array_merge(
		[
			'viewbox'	=> isset( $args['viewbox'] ) ? $args['viewbox'] : null ,
			'viewBox'	=> isset( $args['viewBox'] ) ? $args['viewBox'] : null ,
			'class' 	=> isset( $args['class'] ) ? $args['class'] : null,
			'width' 	=> isset( $args['width'] ) ? $args['width'] : null, 
			'height' 	=> isset( $args['height'] ) ? $args['height'] : null  
		],
		isset( $args['attrs'] ) ? $args['attrs'] : []
	);
	?>
	<svg
		<?php foreach( $attributes as $attribute => $value ) {
			if( null !== $value ) echo esc_attr( $attribute ) ."='".esc_attr( $value )."'";
		} ?>
	>
		<use xlink:href="#<?php echo esc_attr( $args['icon_id'] ) ?>"></use>
	</svg><?php 
}