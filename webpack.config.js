const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const path = require('path');

module.exports = {
    context: path.join(__dirname + '/src/pages'),
    resolve: {
        moduleDirectories: ['node_modules'],
        extensions: ['', '.js'],
        alias: {
            'Components': path.resolve(__dirname, 'src/components/'),
            '@': path.resolve(__dirname, 'src'),
        }
    },
    entry: {
        index: './index',
    },
    watch: false,
    devtool: isDev ? 'cheap-module-inline-source-map' : null,
    module: {
        loaders: [{
            test: /\.js$/,
            include: path.join(__dirname, 'src'),
            loader: 'babel-loader'
        }]
    },
    output: {
        filename: '[name].js',
        publicPath: '/js/'
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        !isDev && new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
            minChunks: 2,
        }),
    ].filter(Boolean)
};