const reflect = document.querySelectorAll('[data-reflect]')

Array.from(reflect).forEach(el => {
    const inputName = el.getAttribute('data-reflect')

    el
        .closest('.wpcf7')
        .addEventListener('wpcf7submit', () => {
            const isInvalid = 
                el
                    .closest('form')
                    .querySelector(`[name="${inputName}[]"]`)
                    .closest('.wpcf7-form-control')
                    .classList
                    .contains('wpcf7-not-valid')

            isInvalid ? el.classList.add('invalid') : el.classList.remove('invalid')
        })
    
     el
        .closest('.wpcf7')
        .addEventListener('wpcf7mailsent', (e) => {
            const eventTarget = e.currentTarget
            const reflectTarget = eventTarget.querySelector(`[name="${inputName}[]"]`)

            console.log(reflectTarget, reflectTarget)

            Array.from(eventTarget.querySelectorAll('[data-reflect]')).forEach(reflectOn => {
                if (reflectTarget) {
                    reflectOn.classList.remove('checked')
                }
            })
            
        })
    
    el.addEventListener('change', e => {
        const eventTarget = e.currentTarget

        const inputName = eventTarget.getAttribute('data-reflect')
        const reflectTarget = eventTarget.closest('.wpcf7').querySelector(`[name="${inputName}[]"]`)

        if (reflectTarget) {
            reflectTarget.checked ? eventTarget.classList.add('checked') : eventTarget.classList.remove('checked')
        }
    })

    el.addEventListener('click', e => {
        const eventTarget = e.currentTarget

        if (eventTarget.checked !== undefined) {
            return false;
        }

        const inputName = eventTarget.getAttribute('data-reflect')
        const reflectTarget = eventTarget.closest('.wpcf7').querySelector(`[name="${inputName}[]"]`)

        if (reflectTarget) {
            reflectTarget.checked = !reflectTarget.checked
            reflectTarget.checked ? eventTarget.classList.add('checked') : eventTarget.classList.remove('checked')
        }
    })
})