import $ from 'jquery'

export default function animateCss($el, animationName, removeClass = true) {

    const animationEnd = (function (el) {
        const animations = {
            animation: 'animationend',
            OAnimation: 'oAnimationEnd',
            MozAnimation: 'mozAnimationEnd',
            WebkitAnimation: 'webkitAnimationEnd',
        }

        for (let t in animations) {
            if (el.style[t] !== undefined) {
                return animations[t]
            }
        }
    })(document.createElement('div'))


    return new window.Promise((resolve, reject) => {

        const handleAnimationEnd = function (e) {

            if (e.target !== e.currentTarget) {
                $el.one(animationEnd, handleAnimationEnd)
                return false
            }

            if (removeClass) {
                $(this).removeClass('animated ' + animationName)
            }

            resolve()
        }

        $el
            .addClass('animated ' + animationName)
            .one(animationEnd, handleAnimationEnd)
    })
}