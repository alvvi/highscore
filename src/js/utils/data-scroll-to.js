import $ from 'jquery'

$('[data-scroll-to]').on('click', function(e) {
    const $scrollTarget = $('#'+$.attr(this, 'data-scroll-to')).eq(0) 

    if($scrollTarget.length > 0) {
        e.preventDefault()

        let scrollTop = $scrollTarget.offset().top - $('.header').outerHeight() + 50
        $('html, body').animate({ scrollTop }, 800)

        return false
    }
})