import $ from 'jquery'
import '@/js/vendor/jquery-ui.js'
import 'jquery-validation'
import Counter from 'Components/Counter/Counter'

const $payment = $('.payment')
const $form = $payment.find('.form')
const $pricePlanSelect = $form.find('.form__select')
const $total = $form.find('.form__total .form__number')
const $subTotal = $form.find('.form__subtotal .form__number')

$form.validate({
    debug: true,
    rules: {
        first_name: 'required',
        last_name: 'required',
        user_email: {
            required: true,
            email: true 
        }
    },
    messages: {
        first_name: 'Поле обязательно для заполнения',
        last_name: 'Поле обязательно для заполнения',
        user_email: {
            required: 'Поле обязательно для заполнения',
            email: 'Пожалуйста, введите валидный e-mail'
        },
    },
    submitHandler: handleSubmit
})

$pricePlanSelect.selectmenu({
    appendTo: $form
})
const counter = new Counter($form.find('.form__counter'), {
    onUpdate: (amount) => {
        const productId = $pricePlanSelect.val()
        const price = window.wpData.products[productId].price

        const total = +price * +amount

        $total.text( total.toLocaleString('ru-RU') )
    }
})

const productId = $pricePlanSelect.val()
const price = window.wpData.products[productId].price
const amount = counter.state.current
$subTotal.text(  +price.toLocaleString('ru-RU')  )
const total = +price * +amount
$total.text( total.toLocaleString('ru-RU') )


$pricePlanSelect.on('selectmenuchange', (e) => {
    const productId = $pricePlanSelect.val()
    const price = window.wpData.products[productId].price
    const amount = counter.state.current

    $subTotal.text(  +price.toLocaleString('ru-RU')  )

    const total = +price * +amount

    $total.text( total.toLocaleString('ru-RU') )
})

function handleSubmit() {
    console.log('is submit actually hapennig?')
    $.post({
        url: $form.attr('action'),
        data: {
            action: 'course_payment',
            quantity: $form.find('[name="quantity"]').val(),
            first_name: $form.find('[name="first_name"]').val(),
            last_name: $form.find('[name="last_name"]').val(),
            user_email: $form.find('[name="user_email"]').val(),
            product: $form.find('[name="product"]').val(),
        }
    })
    .then((res) => {
        const { success, data } = res 
        
        console.log(res)

        if (!data) return false 
        if (!success) {
            console.log('not success!!')
            return 
        }

        $(data.form)
            .insertAfter($form)
            .hide()
            .submit()

    })
    .catch((err) => {
        console.log(err)
    })
}
