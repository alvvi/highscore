import $ from 'jquery'
import '@/js/vendor/jquery-ui.js'

const $callback = $('.callback')
const $callbakcSelects = $callback.find('.form__select')

$callbakcSelects.selectmenu({
    appendTo: $callback.find('.form'),
    maxHeight: 60
})