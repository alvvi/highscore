import $ from 'jquery'

const $accordion = $('.accordion')
const $tabs = $accordion.find('.accordion__tab')
const $header = $tabs.find('.accordion__header')

$header.on('click', function() {
    toggleTab( $(this).closest('.accordion__tab').index() )
})

$(document).keypress(function(e) {
    if (e.which === 13) {
        toggleTab(
            $header.filter(':focus').parent().index()
        )
    }
})

function toggleTab(index) {
    const isActive = $tabs.eq(index).hasClass('accordion__tab--expanded')
    isActive ? closeTab(index) : openTab(index)
}

function openTab(index) {
    const target = $tabs.eq(index)
    closeTab(0, $tabs.length)
    target.addClass('accordion__tab--expanded')

    const $body = target.find('.accordion__body')
    $body.css('max-height', $body[0].scrollHeight +'px')  
}

function closeTab(index, offset) {
    const target = $tabs.slice(index, offset || index+1)
    target.removeClass('accordion__tab--expanded')

    const $body = target.find('.accordion__body')
    $body.css('max-height', 0+'px') 
}

openTab(0)