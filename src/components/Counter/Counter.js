import $ from 'jquery'

export default class Counter {
    constructor(selector, options = {}) {

        this.state = {
            current: 1
        }

        this._options = options 

        this.$counter = $(selector)

        this.$input = this.$counter.find('.counter__display')
        this.$substractBtn = this.$counter.find('.counter__substract')
        this.$addBtn = this.$counter.find('.counter__add')

        this.attachListeners()
    }

    add(n) {
        let next = this.state.current + Number(n)
        if(next < 1) next = 1
        this.state.current = next
    }

    substract(n) {
        let next = this.state.current - Number(n)
        if(next < 1) next = 1
        this.state.current = next
    }

    updateDom() {

        if(this._options.onUpdate) {
            this._options.onUpdate(this.state.current)
        }

        this.$input.val(this.state.current)
    }

    attachListeners() {
        const counter = this
        this.$substractBtn.on('click', function() {
            counter.substract(1)
            counter.updateDom()
        })
        
        this.$addBtn.on('click', function() {
            counter.add(1)
            counter.updateDom()
        })

        this.$input.on('change', function(e) {
            let value = Number( e.currentTarget.value )

            if(value > 1 && !isNaN(value)) {
                counter.state.current = value 
            } 

            counter.updateDom()
        })
    }
}