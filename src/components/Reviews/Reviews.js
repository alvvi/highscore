import $ from 'jquery'
import 'slick-carousel'

const $reviews = $('.reviews')
const $slider = $reviews.find('.reviews__slider')
const $blurSlider = $reviews.find('.reviews__blur-slider')

$slider.slick({
    arrows: false,
    speed: 500,
    asNavFor: $blurSlider,
    arrows: true,
    dots: true,
    infinite: false,
    customPaging: function(_slider, i) {
        return $('<button class="controls__bullet" type="button" />') 
    },
    prevArrow: $reviews.find('.reviews__controls .controls__arrow--prev'),
    nextArrow: $reviews.find('.reviews__controls .controls__arrow--next'),
    appendDots: $reviews.find('.reviews__controls .controls__bullets'),
})

$blurSlider.slick({
    arrows: false,
    fade: true,
    speed: 200, 
    asNavFor: $slider
})

$slider.on('beforeChange', (e, slick, current, next) => {
    const $currentSlide = slick.$slides.eq(current)
    const $video = $currentSlide.find('.reviews__video')
    const $videoFrame = $currentSlide.find('.responsive-video__video')

    if ($videoFrame.length > 0 && $videoFrame[0].YTPlayer) {

        if (typeof $videoFrame[0].YTPlayer.pauseVideo !== 'function') return false 

        $videoFrame[0].YTPlayer.pauseVideo()
    } 

    if ($video.length > 0 && $video[0].pause) {
        $video[0].pause()
    }

})

function onYouTubeIframeAPIReady() {
    $('.responsive-video__video').each((i, el) => {
        el.YTPlayer = new window.YT.Player( $(el)[0], {})
    })
}
window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady