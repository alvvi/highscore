'use strict';

import $ from 'jquery'
import animateCss from '@/js/utils/animateCss'
import NavDrawer from 'Components/NavDrawer/NavDrawer'

const $sideDrawer = $('.side-drawer')
const $overlay = $sideDrawer.find('.side-drawer__overlay')
const $container = $sideDrawer.find('.side-drawer__container')
const $closeBtn = $sideDrawer.find('.side-drawer__close')
const $tabs = $sideDrawer.find('.side-drawer__content-tab')

$tabs.hide()
$tabs.filter('#payment').show()

const drawer = new NavDrawer( $('.side-drawer'), {
        id: 'sideDrawer',
        blockName: 'side-drawer',
        on: {
            'open': (_drawer, e) => {
                e.preventDefault()

                const $eventTarget = $(e.currentTarget)
                const tabId = $eventTarget.attr('data-drawer-tab')

                $tabs.hide()
                $tabs.filter(`#${tabId}`).show()

                if (tabId === 'callback') {
                    let tabHeading = $eventTarget.attr('data-drawer-heading')
                    if (!tabHeading) {
                        tabHeading = 'Обратный звонок'
                    }

                    $('.callback__heading').text(tabHeading)
                    $('.callback__form input[name="form_heading"]').text(tabHeading)
                }
              
                return window.Promise.all([
                    animateCss($overlay, 'fadeIn').then(() => $overlay.css('opacity', '1')),
                    animateCss($container, 'fadeInRight').then(() => $container.css('opacity', '1'))
                ])
                .then(() => {
                    return window.Promise.all([
                        animateCss($closeBtn, 'fadeIn').then(() => $closeBtn.css('opacity', '1')),
                    ])
                })
            },
            'close': (_drawer, e) => {
                e.preventDefault()

                return window.Promise.all([
                    animateCss($closeBtn, 'fadeOut').then(() => $closeBtn.css('opacity', '0')),
                ])
                .then(() => {
                    return window.Promise.all([
                        animateCss($overlay, 'fadeOut').then(() => $overlay.css('opacity', '0')),
                        animateCss($container, 'fadeOutRight').then(() => $container.css('opacity', '0'))
                    ])
                })
            }
        }
    }
)

/**
 * Set inital before animations
 */

if (!drawer.state.opened) {
    $container.css('opacity', '0')
    $overlay.css('opacity', '0')
    $closeBtn.css('opacity', '0')
}

$sideDrawer.find('a').on('click', (e) => {
    const $scrollTarget = $('#'+$.attr(e.currentTarget, 'data-scroll-to')).eq(0) 

    if ($scrollTarget.length > 0) {
        drawer.close(e) 
    }
})