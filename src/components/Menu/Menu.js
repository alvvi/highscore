import $ from 'jquery'
import animateCss from '@/js/utils/animateCss'
import NavDrawer from 'Components/NavDrawer/NavDrawer'

const $menu = $('.menu')
const $overlay = $menu.find('.menu__overlay')
const $container = $menu.find('.menu__container')
const $navList = $menu.find('.menu__list')
const $actions = $menu.find('.menu__actions')
const $closeBtn = $menu.find('.menu__close')

/**
 * Set inital before animations
 */
$container.css('opacity', '0')
$overlay.css('opacity', '0')

$navList.css('opacity', '0')
$closeBtn.css('opacity', '0')
$actions.css('opacity', '0')

const menu = new NavDrawer( $('.menu'), {
        on: {
            'open': () => {
                return window.Promise.all([
                    animateCss($overlay, 'fadeIn').then(() => $overlay.css('opacity', '1')),
                    animateCss($container, 'fadeInLeft').then(() => $container.css('opacity', '1'))
                ])
                .then(() => {
                    return window.Promise.all([
                        animateCss($navList, 'fadeIn').then(() => $navList.css('opacity', '1')),
                        animateCss($closeBtn, 'fadeIn').then(() => $closeBtn.css('opacity', '1')),
                        animateCss($actions, 'fadeInUp').then(() => $actions.css('opacity', '1'))
                    ])
                })
            },
            'close': () => {
                return window.Promise.all([
                    animateCss($navList, 'fadeOut').then(() => $navList.css('opacity', '0')),
                    animateCss($closeBtn, 'fadeOut').then(() => $closeBtn.css('opacity', '0')),
                    animateCss($actions, 'fadeOutDown').then(() => $actions.css('opacity', '0'))
                ])
                .then(() => {
                    return window.Promise.all([
                        animateCss($overlay, 'fadeOut').then(() => $overlay.css('opacity', '0')),
                        animateCss($container, 'fadeOutLeft').then(() => $container.css('opacity', '0'))
                    ])
                })
            }
        }
    }
)

$navList.find('a').on('click', (e) => {
    const $scrollTarget = $('#'+$.attr(e.currentTarget, 'data-scroll-to')).eq(0) 

    if ($scrollTarget.length > 0) {
        menu.close(e) 
    }
})