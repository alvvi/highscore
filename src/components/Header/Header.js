import $ from 'jquery'

const $header = $('.header')
const stickyClass = 'header--sticky'

$(window).on('scroll', (e) => {
    if (window.pageYOffset > 0) {
        $header.addClass(stickyClass)
    } else {
        $header.removeClass(stickyClass)
    } 
})