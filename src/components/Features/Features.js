import $ from 'jquery'
import 'slick-carousel'
import { breakpoints } from '@/js/const'

const $features = $('.features')
const $boxSlider = $features.find('.features__box-slider')
const $laptopSlider = $features.find('.features__laptop-slider')

$boxSlider.slick({
    infinite: false,
    arrows: true,
    dots: true,
    customPaging: function(_slider, i) {
        return $('<button class="controls__bullet" type="button" />') 
    },
    prevArrow: $features.find('.features__controls .controls__arrow--prev'),
    nextArrow: $features.find('.features__controls .controls__arrow--next'),
    appendDots: $features.find('.features__controls .controls__bullets'),
    adaptiveHeight: true,
    fade: true,
    asNavFor: $laptopSlider
})

$laptopSlider.slick({
    infinite: false,
    arrows: false,
    asNavFor: $boxSlider,
    adaptiveHeight: false,
    responsive: [
        {
          breakpoint: breakpoints.$lg,
          settings: {
            adaptiveHeight: true,
          }
        }
    ]
})