import $ from 'jquery'

export default class NavDrawer {
    constructor(drawer, options) {

        this.id = options.id || 'menu'
        this.blockName = options.blockName || 'menu'

        this.classes = {
            opened: options.classes && options.classes.oepned  || `${this.blockName}--opened`,
            overlay: options.classes && options.classes.overlay || `${this.blockName}__overlay`,
            container: options.classes && options.classes.container || `${this.blockName}__container`,
            closeBtn: options.classes && options.classes.closeBtn || `${this.blockName}__close`,
            links: options.classes && options.classes.links || `${this.blockName}__link`,
        }

        this.on = options.on || {}
        
        this.$drawer = $(drawer)
        this.$overlay = this.$drawer.find(this.classes.overlay)
        this.$container = this.$drawer.find(this.classes.container)
        this.$closeBtn = this.$drawer.find(this.classes.closeBtn)

        this.state = {
            opened: this.$drawer.hasClass(this.classes.opened),
            animationPromise: null
        }

        this.attachListners()
    }

    attachListners() {
        $(`[data-open-nav="${this.id}"]`).on('click', this.open.bind(this))
        $(`[data-close-nav="${this.id}"]`).on('click', this.close.bind(this))
        $(document).keyup((e) => {
            if (e.keyCode === 27 && this.state.opened) {
                this.close(e)
            }
        })
        //this.$links.on('click', this.close)
    }

    open(e) {

        //if (this.state.opened) return

        this.$drawer.attr('aria-hidden', false)
        this.$drawer.addClass(this.classes.opened)

        if (typeof this.on['open'] === 'function') {
            const returnValue = this.on['open'](this, e)
            
            Promise.resolve(returnValue).then(() => {
                this.state.opened = true
            })
        } else {
            this.state.opened = true
        }
    }

    close(e) {
        //if (!this.state.opened) return false 

        if ( 
            e != undefined
            && $(e.currentTarget).attr('data-close-nav') === 'self' 
            && e.target !== e.currentTarget 
        ) {
            return false
        }

        this.$drawer.attr('aria-hidden', true)

        if (typeof this.on['close'] === 'function') {

            const returnValue = this.on['close'](this, e)

            Promise.resolve(returnValue).then(() => {
                this.$drawer.removeClass(this.classes.opened)
                this.state.opened = false 
            })

        } else {
            this.$drawer.removeClass(this.classes.opened)
            this.state.opened = false 
        }
    } 
}