import $ from 'jquery'
import 'slick-carousel'
import { breakpoints } from '@/js/const'

const $teachers = $('.teachers')
const $slider = $teachers.find('.teachers__slider')

const slidesAmount = $slider.children().length 
const isEnoughSlides = slidesAmount > 3

if (!isEnoughSlides) 
    $slider.find('.teachers__card').css('justify-content', 'center')

$slider.slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    focusOnSelect: true,
    arrows: true,
    dots: true,
    customPaging: function(_slider, i) {
        return $('<button class="controls__bullet" type="button" />') 
    },
    prevArrow: $teachers.find('.teachers__controls .controls__arrow--prev'),
    nextArrow: $teachers.find('.teachers__controls .controls__arrow--next'),
    appendDots: $teachers.find('.teachers__controls .controls__bullets'),
    adaptiveHeight: true,
    responsive: [
        {
            breakpoint: breakpoints.$xl,
            settings: {
                slidesToShow: isEnoughSlides ? 3 : 1,
                variableWidth: isEnoughSlides,
                centerPadding: 0,
                centerMode: isEnoughSlides,
                initialSlide: isEnoughSlides ? 1 : 0
            }
        }
    ]
})