<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package highscore
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">-->

</head>

<body <?php body_class(); ?>>

<?php get_template_part( 'template-parts/sprite' ); ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hs' ); ?></a>

	<?php
		$header_links = get_field('header_links', 'option');
		$phone = get_field('phone', 'option');
		$normalized_phone = preg_replace('~\D~', '', $phone);
	?>
	<header id="masthead" class="site__header header">
		<div class="header__grid grid">
			<div class="header__row row">
				<div class="header__col header__col--left col col--auto col--lg_5">

					<button data-open-nav="menu" class="header__toggle menu-toggle">
						<span></span>
						<span></span>
						<span></span>
					</button>

					<div class="header__row header__row--links row">
						<?php
						foreach ( $header_links as $link ) : 
							?>
							<div class="header__col col col--auto">
								<a 
									data-scroll-to="<?php echo $link['section_id']; ?>" 
									href="<?php echo esc_url( $link['link']['url']."#{$link['section_id']}" ); ?>" 
									target="<?php echo $link['link']['target']; ?>" 
									class="header__link link"
								>
									<?php echo $link['link']['title']; ?>
								</a> 
							</div>
							<?php
						endforeach;
						?>
					</div>

				</div>
				<div class="header__col header__col--logo col col--auto col--lg_2">
					<a href="<?php echo home_url(); ?>" class="header__logo-wrapper">
						<?php 
							hs_sprite_icon( [
								'icon_id' => 'logo',
								'width'   => '150px',
								'height'  => '28px',
								'viewBox' => '0 0 150 28',
								'class'   => 'header__logo',
								'attrs'   => [
									'fill' => '#fff',
								]
							] );
						?>
					</a>
				</div>
				<div class="header__col header__col--right col col--12 col--sm_6 col--lg_5">
					<div class="header__row row">
						<div class="header__col col col--auto">
							<a href="tel:<?php echo $normalized_phone; ?>" class="header__phone-link">
								<?php 
									hs_sprite_icon( [
										'icon_id' => 'phone',
										'width'   => '15px',
										'height'  => '19px',
										'viewBox' => '0 0 13 19',
										'class'   => 'header__icon',
										'attrs'   => [
											'fill' => '#fff',
										]
									] );
								?>
								<span class="header__phone">
									<?php echo esc_html( $phone ); ?>
								</span> 
							</a>
						</div>
						<div class="header__col col col--auto">
							<button data-drawer-tab="callback" data-open-nav="sideDrawer" class="header__call-btn button button--rounded button--shadow_none">
								<?php 
									hs_sprite_icon( [
										'icon_id' => 'phone',
										'width'   => '15px',
										'height'  => '19px',
										'viewBox' => '0 0 13 19',
										'class'   => 'button__icon',
										'attrs'   => [
											'fill' => '#000',
										]
									] );
								?>
								<span class="button__text">
									Перезвоните мне
								</span>
							</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</header><!-- #masthead -->

	<?php
		$menu_nav = get_field('menu_nav', 'option');
	?>
	<div class="site__menu menu">
		<div class="menu__overlay" data-close-nav=self>
			<div class="menu__container">
				<button class="menu__close" data-close-nav="menu">
					<?php 
						hs_sprite_icon( [
							'icon_id' => 'close',
							'width'   => '16px',
							'height'  => '16px',
							'viewBox' => '0 0 16 16',
							'class'   => 'menu__icon',
							'attrs'   => [
								'fill' => '#000',
							]
						] );
					?>
				</button>
				<nav class="menu__nav" role="navigation">
					<ul class="menu__list">
						<?php
						foreach ( $menu_nav as $menu_item ) :
							?>
							<li class="menu__item">
								<a 
									data-scroll-to="<?php echo esc_attr( $menu_item['section_id'] ); ?>" 
									target="<?php echo $menu_item['link']['target']; ?>" 
									href="<?php echo $menu_item['link']['url']."#{$menu_item['section_id']}"; ?>" 
									class="menu__link"
								>
									<?php echo $menu_item['link']['title']; ?>
								</a>
							</li>
							<?php
						endforeach;
						?>
					</ul>
				</nav>

				<div class="menu__actions">
					<button data-close-nav="menu" data-drawer-tab="payment" data-open-nav="sideDrawer" class="menu__button button button--fluid button--dark button--ghost button--fz_bigger">
						оплатить курс
					</button>
					<button data-close-nav="menu" data-drawer-tab="callback" data-open-nav="sideDrawer" class="menu__button button button--fluid button--shadow_none button--fz_bigger">
						записаться сейчас
					</button>
				</div>
			</div>
		</div>
	</div>

	<aside class="side__aside side-drawer">
		<div class="side-drawer__overlay" data-close-nav=self>
			<div class="side-drawer__container">
				<button class="side-drawer__close" data-close-nav="sideDrawer">
					<?php 
						hs_sprite_icon( [
							'icon_id' => 'close',
							'width'   => '16px',
							'height'  => '16px',
							'viewBox' => '0 0 16 16',
							'class'   => 'side-drawer__icon',
							'attrs'   => [
								'fill' => '#000',
							]
						] );
					?>
				</button>

				<div class="side-drawer__dynamic-content">
					
					<div id="policy" class="side-drawer__content-tab">
						<?php
							$policy_page = get_post( 3 );
							$policy_page_title = get_the_title( $policy_page );
							$policy_content = apply_filters( 'the_content', $policy_page->post_content );
							$policy_content = str_replace( ']]>', ']]&gt;', $policy_content );
						?>
						<article class="policy">
							<header class="policy__header">
								<h2 class="policy__heading heading heading--md">
									<?php echo $policy_page_title; ?>
								</h2>
							</header>
							<div class="policy__content">
								<?php echo do_shortcode( $policy_content );  ?>
							</div>
						</article>
					</div>

					<div id="callback" class="side-drawer__content-tab">
						<div class="callback">
							<h2 class="callback__heading heading heading--md heading--accent">Обратный звонок</h2>
							<?php 
								echo do_shortcode('[contact-form-7 id="186" title="Обратный звонок" html_class="callback__form form form--borders form--inputs_lg"]'); 
							?>
						</div>
					</div>
					<?php
						$wc_products = wc_get_products( [
							'category' => [ 'courses' ],
						] );
					?>
					<div id="payment" class="side-drawer__content-tab">
						<div class="payment">
							<h2 class="payment__heading heading heading--md heading--accent">Оплатить курс</h2>
							<?php wc_print_notices(); ?> 
							<form method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" class="payment__form form form--borders form--inputs_lg">
								<div class="form__fields row">
									<input type="hidden" name="action" value="course_payment">
									<div class="form__col col--12 col--sm_6">
										<label for="" class="form__label">Ваше имя</label>
										<input name="first_name" type="text" class="form__input">
									</div>
									<div class="form__col col--12 col--sm_6">
										<label for="" class="form__label">Ваша фамилия</label>
										<input name="last_name" type="text" class="form__input">
									</div>
									<div class="form__col col--12">
										<label for="" class="form__label">Ваш e-mail</label>
										<input name="user_email" type="text" class="form__input">
									</div>
									<div class="form__col col--12 col--sm_8">
										<label for="" class="form__label">Выберите тариф</label>
										<select name="product" class="form__select">
											<?php
											foreach ( $wc_products as $product ) : 
												?>
												<option value="<?php echo $product->get_id(); ?>">
													<?php echo $product->get_title(); ?>
												</option>
												<?php
											endforeach;
											?>
										</select>
									</div>
									<div class="form__col col--12 col--sm_4">
										<label for="" class="form__label">Кол-во месяцев</label>
										<div class="form__counter counter">
											<div class="counter__substract"></div>
											<input class="counter__display" name="quantity" value="1" type="number">
											<div class="counter__add"></div>
										</div>
									</div>
								</div>

								<div class="form__subtotal row row--gutters_none">
									<div class="form__sum-label col--12 col--lg_auto form__sum-label">
										Стоимость тарифа за один месяц
									</div>
									<div class="form__dots col--12 col--lg_grow">

									</div>
									<div class="form__sum">
										<span class="form__number">0</span> руб./мес.
									</div>
								</div>

								<div class="form__total row row--gutters_none">
									<div class="form__sum-label col--12 col--lg_auto form__sum-label">
										Итого
									</div>
									<div class="form__dots col--12 col--lg_grow">

									</div>
									<div class="form__sum">
										<span class="form__number">0</span>  руб.
									</div>
								</div>

								<div class="form__submit">
									<button class="form__button button button--fluid button--fz_bigger">Отправить</button>
								</div>

								<div class="form__info">
									<a href="#" data-scroll-to="prices" class="form__link link link--doc">
									<?php hs_sprite_icon( [
											'icon_id' => 'info',
											'width'   => '30px',
											'height'  => '32px',
											'viewBox' => '0 0 30 32',
											'class'   => 'link__icon',
											'attrs'   => [
												'fill' => '#000',
											]
										] ); ?>Подробнее о тарифах
									</a>
								</div>
							</form>
						</div>
						<?php echo do_shortcode('[woocommerce_checkout]'); ?>
					</div>

				</div>

			</div>
		</div>
	</aside>

	<div id="content" class="site__content">

	<?php 
		$frontpage_id = (int) get_option( 'page_on_front' );

		$hero_heading = get_field('hero_heading', $frontpage_id);
		$hero_subheading = get_field('hero_subheading', $frontpage_id);
		$hero_bg = get_field('hero_bg', $frontpage_id);

		if ( get_field('hero_heading') ) {
			$hero_heading = get_field('hero_heading');
		}

		if ( get_field('hero_subheading') ) {
			$hero_subheading = get_field('hero_subheading');
		}

		if ( get_field('hero_bg') ) {
			$hero_bg = get_field('hero_bg');
		}
	?>
	<section id="hero" style="background-image:url(<?php echo $hero_bg['url']; ?>);" class="hero <?php if ( is_front_page() || is_page( [193, 195] ) ) echo 'hero--home'; ?>">
		<div class="hero__grid grid">
			<div class="hero__row">
				<div class="hero__content col col--12">
					<?php
						if ( ! is_front_page() ) : 
							?>
							<a class="hero__link link link--doc" href="<?php echo home_url(); ?>">
								<?php hs_sprite_icon( [
									'icon_id' => 'arrow-back',
									'width'   => '10px',
									'height'  => '18px',
									'viewBox' => '0 0 10 18',
									'class'   => 'link__icon',
									'attrs'   => [
										'fill' => '#fff',
									]
								] ); ?>
								Назад
							</a>
							<?php 
						endif;
					?>
					<h1 class="hero__heading heading heading--lg"><?php echo $hero_heading; ?></h1>
					<p class="hero__subheading subheading"><?php echo $hero_subheading; ?></p>
					<?php 
					if (is_page( [193, 195] ) ) :
						?>
						<div class="hero__actions">
							<a href="<?php echo home_url(); ?>" class="hero__button button button--fz_bigger">На главную</a>
						</div>
						<?php
					endif;
					?>
				</div>
			</div>
		</div>
	</section>
