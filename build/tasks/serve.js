const browserSync = require('browser-sync').create();

module.exports = (options) => {
    return () => {
        browserSync.init({
            proxy: options.url,
            server: options.server 
        });
        
        browserSync
            .watch(options.watchDir)
            .on('change', browserSync.reload);
    };
};