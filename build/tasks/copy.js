const gulp = require('gulp');

/**
 * Copy files from src to dest
 * @param  {object} options An options object, must contain at least
 *                          src and dest property
 * @return {function}       Function to be passed inside the task method
 */
module.exports = (options) => {
    return () => 
         gulp.src(options.src)
            .pipe(gulp.dest(options.dest));
};
