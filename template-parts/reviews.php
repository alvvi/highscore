<?php
$frontpage_id = (int) get_option( 'page_on_front' );

$reviews_heading = get_field('reviews_heading');
$reviews_subheading = get_field('reviews_subheading');
$reviews_slides = get_field('reviews_slides');

if ( ! $reviews_heading ) {
	$reviews_heading = get_field('reviews_heading', $frontpage_id);
}

if ( ! $reviews_subheading ) {
	$reviews_subheading = get_field('reviews_subheading', $frontpage_id);
}

if ( ! $reviews_slides ) {
	$reviews_slides = get_field('reviews_slides', $frontpage_id);
}

if ( $reviews_slides ) : 
	?>
	<section id="reviews" class="reviews section">
		<div class="reviews__grid grid">
			<h2 class="reviews__heading heading heading--md"><?php echo $reviews_heading; ?></h2>
			<h3 class="reviews__subheading subheading"><?php echo $reviews_subheading; ?></h3>
		</div>

		<div class="reviews__sliders-wrapper">
			<div class="reviews__grid grid">
				<div class="reviews__slider">
				<?php 
				foreach ( $reviews_slides as $slide ) : 
					$video_embed = $slide['video_embed'];
					$video_html = $slide['video_html'];
					?>
					<div class="reviews__slide">
						<?php 
						if ( false && $slide['preview'] ) : 
							?>
							<img src="<?php echo $slide['preview']['url']; ?>" alt="<?php echo $slide['preview']['alt']; ?>" class="reviews__img responsive-img">
							<button class="reviews__play play">
								<?php 
									hs_sprite_icon( [
										'icon_id' => 'play-btn',
										'width'   => '100px',
										'height'  => '100px',
										'viewBox' => '0 0 100 100',
										'class'   => 'play__icon',
										'attrs'   => [
											'fill' => '#000',
										]
									] );
								?>
							</button>
							<?php
						endif;

						if ( ! $video_html && $video_embed ) :
							// use preg_match to find iframe src
							preg_match('/src="(.+?)"/', $video_embed, $matches);
							$src = $matches[1];
							$params = [
								'controls'          => 1,
								'enablejsapi'       => 1,
								'rel'               => 0,
								'modestbranding'    => 1,
								'mute'              => 0
							];
							$new_src = add_query_arg($params, $src);
							$video_embed = str_replace($src, $new_src, $video_embed);

							// add extra attributes to iframe html
							$video_embed_attributes = 'class="responsive-video__video" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen';
							$video_embed = str_replace('></iframe>', ' ' . $video_embed_attributes . '></iframe>', $video_embed);
							?>
							<div class="reviews__video responsive-video">
								<?php echo $video_embed; ?>
							</div>
							<?php
						endif;

						if ( $video_html ) :
							?>
							<video poster="<?php echo $slide['preview']['url']; ?>" class="reviews__video"  controls>
								<?php 
								if ( $video_html['webm'] ) :
									 ?>
									<source class=reviews__source src="<?php echo $video_html['webm']['url']; ?>" type=video/webm  />
									<?php 
								endif; 
						
								if ( $video_html['mp4'] ) : 
									?>
									<source class=reviews__source src="<?php echo $video_html['mp4']['url']; ?>"  type=video/mp4 />
									<?php 
								endif; 
								?>
							</video>
							<?php 
						endif;
						?>
					</div>
					<?php
				endforeach;
				?>
				</div>
			</div>

			<div class="reviews__blur-slider">
			<?php 
			foreach ( $reviews_slides as $slide ) : 
				$video_embed = $slide['video_embed'];
				?>
				<div class="reviews__slide">
					<div class="reviews__grid grid">
					<?php 
						if ( $slide['preview'] ) : 
							?>
							<img src="<?php echo $slide['preview']['url']; ?>" alt="<?php echo $slide['preview']['alt']; ?>" class="reviews__img responsive-img">
							<?php
						endif;

						if ( $video_embed ) :

							preg_match('/src="(.+?)"/', $video_embed, $matches);
							$src = $matches[1];
							$parsed_url = parse_url($src);
							$video_embed_id = str_replace('/embed/', '', $parsed_url['path']);

							?>
							<div class="reviews__video responsive-video">
								<img class="reviews__blured-img responsive-img" src="https://img.youtube.com/vi/<?php echo $video_embed_id; ?>/hqdefault.jpg" alt="">
							</div>
							<?php
						endif;
						?>
					?>
					</div>
				</div>
				<?php
			endforeach;
			?>
			</div>
		</div>

		<div class="reviews__grid grid">
			<div class="reviews__controls controls controls--dark">
				<button class="controls__arrow controls__arrow--prev">
					<?php 
						hs_sprite_icon( [
							'icon_id' => 'arrow-ghost',
							'width'   => '50px',
							'height'  => '50px',
							'viewBox' => '0 0 50 50',
							'class'   => 'controls__icon',
							'attrs'   => [
								'fill-opacity' => '1',
								'fill' => '#000',
								'color' => '#fff'
							]
						] );
					?>
				</button>
				<div class="controls__bullets"></div>
				<button class="controls__arrow controls__arrow--next">
					<?php 
						hs_sprite_icon( [
							'icon_id' => 'arrow-ghost',
							'width'   => '50px',
							'height'  => '50px',
							'viewBox' => '0 0 50 50',
							'class'   => 'controls__icon',
							'attrs'   => [
								'fill-opacity' => '1',
								'fill' => '#000',
								'color' => '#fff'
							]
						] );
					?>
				</button>
			</div>
		</div>
	</section>
	<?php
endif;
?>