<?php
$frontpage_id = (int) get_option( 'page_on_front' );
$prices_heading = get_field('prices_heading');
$prices_text = get_field('prices_text');
$prices_list = get_field('prices_list');
$price_postfix = get_field('price_postfix');


if ( ! $prices_heading ) {
    $prices_heading = get_field('prices_heading', $frontpage_id);
}
if ( ! $prices_text ) {
    $prices_text = get_field('prices_text', $frontpage_id);
}
if ( ! $prices_list ) {
    $prices_list = get_field('prices_list', $frontpage_id);
}
if ( ! $price_postfix ) {
    $price_postfix = get_field('price_postfix', $frontpage_id);
}
?>
<section id="prices" class="prices section">
	<div class="prices__grid grid">
		<h2 class="prices__heading heading heading--md"><?php echo $prices_heading; ?></h2>
		<p class="prices__text"><?php echo $prices_text; ?></p>

		<div class="prices__row row">
		<?php 
		foreach ( $prices_list as $price_item ) :
			$product = wc_get_product( $price_item['product_id'] );
			$product_title = $product->get_title();
			$product_price = $product->get_price();
			$product_features_list = get_field('wc_product_features', $price_item['product_id']);
			?>
			<div class="prices__col col col--12 col--sm_6 col--lg_3">
				<article class="prices__card prices-card">
					<header class="prices-card__header">
						<h3 class="prices-card__name heading heading--sm"><?php echo $product_title; ?></h3>
						<div class="prices-card__price heading heading--sm"><?php echo $product_price . " " . $price_postfix; ?></div>
					</header>
					<?php 
					if ( $product_features_list ) :
						?>
						<ul class="prices-card__list">
						<?php
							foreach ( $product_features_list as $feature ) :
								?>
								<li class="prices-card__item">
									<?php 
									if ( $feature['label'] ) : 
										?>
										<div class="prices-card__label"><?php echo $feature['label']; ?></div>
										<?php 
									endif; 
									echo $feature['text'];
									?>
								</li>
								<?php
							endforeach;
							?>
						</ul>
						<?php
					endif;
					?>
					<footer class="prices-card__footer">
						<button data-drawer-heading="Заказ на <?php echo $product_title; ?>" data-open-nav="sideDrawer" data-drawer-tab="callback" class="prices-card__button button">Попробовать бесплатно</button>
					</footer>
				</article>
			</div>
			<?php
		endforeach;
		?>
		</div>

		<div class="prices__actions">
			<button data-open-nav="sideDrawer" data-drawer-tab="payment" class="button button--fz_bigger button--shadow button--fluid">
				<img src="<?php echo THEME_ASSETS ?>/img/icons/card.svg" alt="" class="button__icon">
				оплатить курс сейчас
			</button>
		</div>
	</div>
</section>