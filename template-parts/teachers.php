<?php
$frontpage_id = (int) get_option( 'page_on_front' );
$teachers_heading = get_field('teachers_heading', $frontpage_id);
$teachers_slides = get_field('teachers_slides', $frontpage_id);

if ( get_field('teachers_slides') ) {
	$teachers_slides = get_field('teachers_slides');
}

?>
<section id="teachers" class="teachers section">
	<div class="teachers__grid grid">
		<h2 class="teachers__heading heading heading--md"><?php echo $teachers_heading; ?></h2>
		<div class="teachers__slider row">

		<?php 
		foreach ( $teachers_slides as $slide ) :
			?>
			<div class="teachers__slide col col--12">

				<div class="teachers__card teacher-card">

					<div class="teacher-card__preview" style="background-image:url(<?php echo $slide['img']['sizes']['teacher-photo']; ?>);">
						<h3 class="teacher-card__name heading heading--sm"><?php echo $slide['name']; ?></h3>
						<div class="teacher-card__position"><?php echo $slide['position']; ?></div>
					</div>

					<?php 
					if ( $slide['info_blocks'] ) :
						?>
						<div class="teacher-card__body">
							<?php 
							foreach ( $slide['info_blocks'] as $info_block ) :
								?>
								<div class="teacher-card__info">
									<div class="teacher-card__label"><?php echo $info_block['label']; ?></div>
									<p class="teacher-card__text"><?php echo $info_block['value']; ?></p>
								</div>
								<?php
							endforeach;
							?>
						</div>
						<?php
					endif;
					?>

				</div>

			</div>
			<?php 
		endforeach; 
		?>

		</div>

		<div class="teachers__controls controls">
			<button class="controls__arrow controls__arrow--prev">
				<?php 
					hs_sprite_icon( [
						'icon_id' => 'arrow-ghost',
						'width'   => '50px',
						'height'  => '50px',
						'viewBox' => '0 0 50 50',
						'class'   => 'controls__icon',
						'attrs'   => [
							'fill-opacity' => '0',
							'fill' => 'none',
							'color' => '#000'
						]
					] );
				?>
			</button>
			<div class="controls__bullets"></div>
			<button class="controls__arrow controls__arrow--next">
				<?php 
					hs_sprite_icon( [
						'icon_id' => 'arrow-ghost',
						'width'   => '50px',
						'height'  => '50px',
						'viewBox' => '0 0 50 50',
						'class'   => 'controls__icon',
						'attrs'   => [
							'fill-opacity' => '0',
							'fill' => 'none',
							'color' => '#000'
						]
					] );
				?>
			</button>
		</div>
	</div>
</section>