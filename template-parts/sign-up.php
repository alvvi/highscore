<section class="sign-up">
	<div class="sign-up__heading-row row">
		<div class="sign-up__col col col--12 offset--lg_1 col--lg_10">
			<h2 class="sign-up__heading subheading">
				Записаться на бесплатное пробное занятие!
			</h2>
		</div>
	</div>

	<?php 
		echo do_shortcode('[contact-form-7 id="187" title="Записаться на бесплатное пробное занятие" html_class="sign-up__form form"]');
	?>
</section>