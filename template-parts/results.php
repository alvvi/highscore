<?php
$frontpage_id = (int) get_option( 'page_on_front' );
$results_heading = get_field('results_heading');
$results_list = get_field('results_list');

if ( ! $results_heading ) {
    $results_heading = get_field('results_heading', $frontpage_id);
}
if ( ! $results_list ) {
    $results_list = get_field('results_list', $frontpage_id);
}

if ( $results_list ) : 
	?>
	<section id="results" class="results section">
		<div class="results__grid grid">
			<h2 class="results__heading heading heading--md"><?php echo $results_heading; ?></h2>
			<div class="results__row row">
			<?php 
			foreach ( $results_list as $item ) : 
				?>
				<div class="results__col col col--12 col--md_6 col--lg_3">
					<img src="<?php echo $item['img']['url']; ?>" alt="<?php echo $item['img']['alt']; ?>" class="results__icon responsive-img">
					<h3 class="results__heading heading heading--sm"><?php echo $item['heading']; ?></h3>
					<p class="results__text"><?php echo $item['text']; ?></p>
				</div>
				<?php 
			endforeach; 
			?>
			</div>
		</div>

		<?php 
			$career_path_icon = get_field('career_path_icon', $frontpage_id);
			$career_path_text = get_field('career_path_text', $frontpage_id);
			$career_path_button = 'Профориентация';
			$careear_path_url = 'http://highscore-profi.ru';

			if ( is_single() ) {
				$career_path_text = '<strong>Нужны другие предметы?</strong>';
				$career_path_button = 'Попробовать бесплатно';
				$careear_path_url = 'http://highscore-online.ru';
			}
		?>
		<section class="results__career-path career-path">
			<div class="career-path__grid grid">
				<div class="career-path__row row">
					<div class="career-path__col col col--12 col--md_2 col--lg_auto">
						<div class="career-path__icon-wrapper">
							<img src="<?php echo $career_path_icon['url']; ?>" alt="<?php echo $career_path_icon['alt']; ?>" class="career-path__icon responsive-img">
						</div>
					</div>
					<div class="career-path__col col col--12 col--md_10 col--lg_7">
						<p class="career-path__text"><?php echo $career_path_text; ?></p>
					</div>
					<div class="career-path__col col col--12 col--md_10 offset--md_2 offset--lg_0 col--lg_auto">
						<a href="<?php echo $careear_path_url; ?>" target="_blank" class="career-path__button button button--shadow button--fz_bigger">
							<?php echo $career_path_button; ?>
						</a>
						<!--<button data-open-nav="sideDrawer" data-drawer-tab="callback" data-drawer-heading="Профориентация" class="career-path__button button button--shadow button--fz_bigger">Профориентация</button>-->
					</div>
				</div>
			</div>
		</section>

	</section>
	<?php 
endif; 
?>
