<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package highscore
 */

get_header();

$frontpage_id = (int) get_option( 'page_on_front' );

$after_hero_icon_list = get_field('after_hero_icon_list', $frontpage_id);
?>
<section id="after-hero" class="section after-hero">
	<div class="after-hero__grid grid">

		<div class="after-hero__sign-up">
			<?php get_template_part('template-parts/sign-up'); ?>
		</div>

		<?php
		if ($after_hero_icon_list) :
			?>
			<ul class="after-hero__icon-list icon-list">
				<?php
				foreach ( $after_hero_icon_list as $item ) :
					?>
					<li class="icon-list__item row">
						<div class="icon-list__col col col--12 col--md_2 col--xl_1 offset--xl_1">
							<img src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['icon']['alt']; ?>">
						</div>
						<div class="icon-list__col col col--12 col--md_4">
							<strong class="icon-list__heading"><?php echo $item['heading']; ?></strong>
						</div>
						<div class="icon-list__col col col--12 col--md_6 col--xl_5">
							<p class="icon-list__text"><?php echo $item['text']; ?></p>
						</div>
					</li>
					<?php
				endforeach;
				?>
			</ul>
			<?php
		endif;
		?>

		<div class="after-hero__actions">
			<a href="<?php echo home_url() . '#courses';  ?>" class="dib after-hero__button button">
				Нужны другие предметы
			</a>
		</div>
		
	</div>
</section>

<?php 
$features_heading = get_field('features_heading', $frontpage_id);
$features_slides = get_field('features_slides', $frontpage_id);
?>
<section id="features" class="section features">
	<div class="features__grid grid">
		<h2 class="features__heading heading--md"><?php echo $features_heading; ?></h2>
		<div class="features__slider">

			<div class="features__laptop">
				<div class="features__laptop-slider">
				<?php 
				foreach ( $features_slides as $index => $slide ) : 
					if ( $index === 0 ) : 
						?>
						<div>
							<div class="features__laptop-wrapper">
								<img src="<?php echo THEME_ASSETS ?>/img/laptop-mask.png" alt="" class="features__img responsive-img">
								<div class="features__laptop-content-wrapper">
									<div 
										style="background-image:url(<?php echo $slide['screen']['url']; ?>);" 
										class="features__laptop-content"
									></div>
								</div>
							</div>
						</div>
						<?php 
					else :
					?>
						<div>
							<img class="features__img-slide responsive-img" src="<?php echo $slide['screen']['url']; ?>" alt="<?php echo $slide['screen']['alt']; ?>">
						</div>
					<?php 
					endif;
				endforeach; 
				?>
				</div>
			</div>

			<div class="features__box">
				<div class="features__box-slider">
					<?php 
					foreach ( $features_slides as $slide ) : 
						?>
						<div class="features__slide">
							<h3 class="features__box-heading heading heading--sm"><?php echo $slide['heading']; ?></h3>
							<?php echo $slide['content']; ?>
						</div>
						<?php 
					endforeach; 
					?>
				</div>
				<div class="features__controls controls">
					<button class="controls__arrow controls__arrow--prev">
						<?php 
							hs_sprite_icon( [
								'icon_id' => 'arrow-ghost',
								'width'   => '50px',
								'height'  => '50px',
								'viewBox' => '0 0 50 50',
								'class'   => 'controls__icon',
								'attrs'   => [
									'fill-opacity' => '0',
									'fill' => 'none',
									'color' => '#000'
								]
							] );
						?>
					</button>
					<div class="controls__bullets"></div>
					<button class="controls__arrow controls__arrow--next">
						<?php 
							hs_sprite_icon( [
								'icon_id' => 'arrow-ghost',
								'width'   => '50px',
								'height'  => '50px',
								'viewBox' => '0 0 50 50',
								'class'   => 'controls__icon',
								'attrs'   => [
									'fill-opacity' => '0',
									'fill' => 'none',
									'color' => '#000'
								]
							] );
						?>
					</button>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_template_part('template-parts/results');
get_template_part('template-parts/teachers');
?>

<?php
$numbers_main_list = get_field('numbers_main_list', $frontpage_id);
$numbers_points_list = get_field('numbers_points_list', $frontpage_id);

if ( ! empty( get_field('numbers_main_list') ) ) {
	$numbers_main_list = get_field('numbers_main_list');
}

if ( ! empty( get_field('numbers_points_list') ) ) {
	$numbers_points_list = get_field('numbers_points_list');
}


?>
<section id="numbers" class="numbers section">
	<div class="numbers__grid grid">
		
		<?php
		if ( $numbers_main_list ) : 
			?>
			<div class="numbers__main-list row">
			<?php
			foreach ( $numbers_main_list as $number_list_item ) : 
				?>
				<div class="numbers__item col col--12 col--lg_6">
					<div class="numbers__circle">
						<div class="numbers__number"><?php echo $number_list_item['number']; ?></div>
					</div>
					<p class="numbers__text"><?php echo $number_list_item['text']; ?></p>
				</div>
				<?php
			endforeach;
			?>
			</div>
			<?php
		endif;
	
		if ( $numbers_points_list ) : 
			?>
			<div class="numbers__points">
				<h3 class="numbers__heading heading--sm">Средние баллы учеников</h3>
				<ul class="numbers__list row">
				<?php
				foreach ( $numbers_points_list as $number_list_item ) : 
					?>
					<li class="numbers__item col col--6 col--md_4 col--lg_2">
						<div class="numbers__circle">
							<div class="numbers__number"><?php echo $number_list_item['number']; ?></div>
						</div>
						<div class="numbers__subject"><?php echo $number_list_item['subject_name']; ?></div>
					</li>
					<?php
				endforeach;
				?>
				</ul>
			</div>
			<?php
		endif;
		?>

	</div>
</section>

<?php
$guarantee_heading = get_field('guarantee_heading', $frontpage_id);
$guarantee_list = get_field('guarantee_list', $frontpage_id);

if ( $guarantee_list ) : 
	?>
	<section id="guarantee" class="guarantee section">
		<div class="guarantee__grid grid">
			<h2 class="guarantee__heading heading--md"><?php echo $guarantee_heading; ?></h2>
			<ul class="guarantee__list row">
				<?php 
				foreach ( $guarantee_list as $guarantee_item ) : 
					?>
					<li class="guarantee__item col col--12 col--lg_6 col--xl_5">
						<h3 class="guarantee__item-heading heading heading--sm"><?php echo $guarantee_item['heading']; ?></h3>
						<p><?php echo $guarantee_item['text']; ?></p>
					</li>
					<?php
				endforeach;
				?>
			</ul>
		</div>
	</section>
	<?php 
endif; 
?>

<?php 
$how_heading = get_field('how_heading', $frontpage_id);
$how_icon_list = get_field('how_icon_list', $frontpage_id);

if ( $how_icon_list ) :
	?>
	<section id="how" class="how section">
		<div class="how__grid grid">
			<h2 class="how__heading heading heading--md"><?php echo $how_heading; ?></h2>
			<ul class="how__icon-list icon-list">
			<?php 
			foreach ( $how_icon_list as $icon_item ) : 
				?>
				<li class="icon-list__item row">
					<div class="icon-list__col col col--12 col--md_2 col--xl_1 offset--xl_1">
						<img src="<?php echo $icon_item['icon']['url']; ?>" alt="<?php echo $icon_item['icon']['alt']; ?>">
					</div>
					<div class="icon-list__col col col--12 col--md_5 col--xl_5">
						<strong class="icon-list__heading"><?php echo $icon_item['heading']; ?></strong>
					</div>
					<div class="icon-list__col col col--12 col--md_5 col--xl_4">
						<p class="icon-list__text"><?php echo $icon_item['text']; ?></p>
					</div>
				</li>
				<?php
			endforeach;
			?>
			</ul>
		</div>
	</section>
	<?php
endif;
?>

<?php
get_template_part('template-parts/reviews');
get_template_part('template-parts/prices');
?>

<?php
	$faq_heading = get_field('faq_heading', $frontpage_id);
	$faq = get_field('faq', $frontpage_id);
?>
<section id="faq" class="faq section">
	<div class="faq__grid grid">
		<h2 class="faq__heading heading heading--md"><?php echo $faq_heading; ?></h2>
		<div class="faq__accordion accordion">
		<?php 
		foreach ( $faq as $index => $faq_row ) : 
			?>
			<section class="accordion__tab">
				<div class="accordion__side"></div>
				<header class="accordion__header" role="button" tabindex="0">
					<h3 class="accordion__title heading heading heading--sm"><?php echo $faq_row['question']; ?></h3>
					<div class="accordion__side">
						<?php
							hs_sprite_icon( [
								'icon_id' => 'accordion-btn',
								'width'   => '50px',
								'height'  => '50px',
								'viewBox' => '0 0 50 50',
								'class'   => 'accordion__icon',
								'attrs'   => [
									'fill' => '#F5F6F8',
								]
							] );
						?>
					</div>
				</header>
				<div class="accordion__body"><?php echo $faq_row['answer']; ?></div>
			</section>
			<?php
		endforeach;
		?>
		</div>
	</div>
</section>

<?php
get_footer();
