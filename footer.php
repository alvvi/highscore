<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package highscore
 */

?>

	</div><!-- #content -->

	<?php 
	$frontpage_id = (int) get_option( 'page_on_front' );
	$join_us_heading = get_field('join_us_heading', $frontpage_id);
	$join_us_subheading = get_field('join_us_subheading', $frontpage_id);

	if ( ! is_page( [193, 195] )  ) :
		?>
		<section class="join-us">
			<div class="join-us__bg">
				<div class="join-us__grid grid">
					<h2 class="join-us__heading heading heading--lg"><?php echo $join_us_heading; ?></h2>
					<h3 class="join-us__subheading subheading"><?php echo $join_us_subheading; ?></h3>
				</div>
			</div>
		</section>
		<?php
	endif;
	?>

	<?php
		$copyright = get_field('copyright', 'option');
		$phone = get_field('phone', 'option');
		$normalized_phone = preg_replace('~\D~', '', $phone);
		$commercial_info = get_field('commercial_info', 'option');
	?>
	<footer id="site-footer" class="site__footer footer">
		<?php
		if ( ! is_page( [193, 195] )  ) :
			?>
			<div class="footer__sign-up">
				<div class="footer__grid grid">
					<?php get_template_part('template-parts/sign-up'); ?>
				</div>
			</div>
			<?php
		endif;
		?>

		<div class="footer__grid grid">
			
			<div class="footer__row row">
				<div class="footer__col footer__col--left col col--12 col--lg_5 col--xl_4">
					<a href="#" data-open-nav="sideDrawer" data-drawer-tab="policy" target="_blank" class="footer__link link link--doc">
						<?php
							hs_sprite_icon( [
								'icon_id' => 'doc',
								'width'   => '15px',
								'height'  => '18px',
								'viewBox' => '0 0 15 18',
								'class'   => 'link__icon',
								'attrs'   => [
									'fill' => '#353535',
								]
							] );
						?>
						Политика конфиденциальности
					</a>
				</div>
				<div class="footer__col footer__col--copyright col col--12 col--lg_2">
					<?php echo $copyright; ?>
				</div>
				<div class="footer__col footer__col--logo col col--12 col--lg_5 col--xl_4">
					<a target="_blank" href="http://360-media.ru/">
						<span>Made by</span>
						<img class="footer__logo responsive-img" src="<?php echo THEME_ASSETS ?>/img/360.jpg" alt="">
					</a>
				</div>
			</div>
			
			<?php
			if ( $commercial_info ) :
				?>
				<div class="footer__row footer__row--info">
					<div class="footer__col col col--12">
						<p class="footer__commercial">
							<?php echo $commercial_info ?> 
						</p>
					</div>
				</div>
				<?php
			endif;
			?>
			
		</div>
	</footer><!-- #site-footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50278039 = new Ya.Metrika2({
                    id:50278039,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50278039" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125481861-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125481861-1');
</script>


<!-- Facebook Pixel Code --> 
<script> 
!function(f,b,e,v,n,t,s) 
{if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
n.callMethod.apply(n,arguments):n.queue.push(arguments)}; 
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0'; 
n.queue=[];t=b.createElement(e);t.async=!0; 
t.src=v;s=b.getElementsByTagName(e)[0]; 
s.parentNode.insertBefore(t,s)}(window,document,'script', 
'https://connect.facebook.net/en_US/fbevents.js'); 
fbq('init', '320068222090407'); 
fbq('track', 'PageView'); 
</script> 
	
<?php if(is_page('order-success')): ?> 
<script type="text/javascript">
	function MetricsCounter() {
    	yaCounter50278039.reachGoal('order'); 
		gtag('event', 'order', { 'event_category': 'order', 'event_action': 'send', });
    }
    window.onload = MetricsCounter;
	
	fbq('track', 'Purchase'); 
</script>
<?php endif; ?>

<noscript> 
<img height="1" width="1" 
src="https://www.facebook.com/tr?id=320068222090407&ev=PageView 
&noscript=1"/> 
</noscript> 
<!-- End Facebook Pixel Code -->

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
	yaCounter50278039.reachGoal('zayavka');
	gtag('event', 'sendemail', { 'event_category': 'mail', 'event_action': 'send', });
    fbq('track', 'Lead');
}, false );
</script>

</body>
</html>
